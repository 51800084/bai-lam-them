import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:snack_restaurant/views/routes/page_route.dart';
import '../../../controllers/handle_logout.dart';
import '../../../validate/validation.dart';

class ProfilePage extends StatefulWidget {
  static const route = '/main';

  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with CommonValidation {
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    final user = auth.currentUser?.email;
    return Scaffold(
        appBar: AppBar(
          title: Text(user == null ? "Hi" : "Hi $user"),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.indigo,
          foregroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              editUserInfoButton(),
              changeUserPasswordButton(),
              logoutButton(),
            ],
          ),
        ));
  }

  Widget editUserInfoButton() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.pushNamed(context, PageRoutes.editProfile);
        },
        child: Text(
          'Edit info'.toUpperCase(),
          style: const TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget changeUserPasswordButton() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.pushNamed(context, PageRoutes.changePassword);
        },
        child: Text(
          'Change password'.toUpperCase(),
          style: const TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget logoutButton() {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      width: 120,
      height: 38,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        ),
        onPressed: () {
          if (auth.currentUser!.email != null) {
            HandleLogout().logOut();
            Navigator.popAndPushNamed(context, PageRoutes.home);
          } else {
            print('No way home');
          }
        },
        child: Text('Log Out'.toUpperCase()),
      ),
    );
  }
}
